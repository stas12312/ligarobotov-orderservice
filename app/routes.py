import json
import os

import requests
from flask import render_template, request

from app import app
from config import basedir

HOST_URL = os.environ.get('HOST')


@app.route('/')
def index():
    # Проверяем, есть ли ссылка, куда отправлять результат
    callback = request.args.get('callback')

    callback_string = ''
    if callback:
        callback_string = f'?callback={callback}'

    return render_template('index.html', title='Выбор набора', callback=callback_string)


@app.route('/<set_type>/', methods=['POST', 'GET'])
def new_order(set_type):
    """Создание нового заказа"""
    if set_type == 'ev3':
        filename = 'ev3.json'
        set_name = 'EV3'
    elif set_type == 'wedo2':
        filename = 'wedo2.json'
        set_name = 'WeDo2.0'
    else:
        return 'Error'
    # Парсим файл с информацией о деталях и преобразуем в типы Python
    json_data = open(os.path.join(basedir, filename), 'r', encoding='UTF-8').read()
    data = json.loads(json_data)
    # Получаем hostname для сервера
    hostname = 'https://order.ligajournal.ru/'

    # Проверяем, есть ли url для отправки результата
    callback = request.args.get('callback')
    callback_string = ''
    if callback:
        callback_string = f'?callback={callback}'

    if request.method == 'POST':
        result = {'list_detail': [],
                  'set_number': request.form['set_number'],
                  'set_name': set_name}  # Список с недостоющими деталями
        for key, value in list(request.form.to_dict().items())[1:]:
            # Вычисляем количество недостающих деталей
            detail = next(item for item in data if item["id"] == int(key))
            diff = int(detail['count']) - int(value)
            # Если нехватает деталей, добавляем информацию в список
            if diff > 0:
                result['list_detail'].append({
                    'id': int(key),
                    'name': detail['name'],
                    'image': f"{hostname}static/img/{set_type}/{detail['image']}",
                    'need_count': diff,
                    'total_count': int(detail['count']),
                    'fact_count': int(value)
                })
        msg = 'Заказ создан, но никуда не отправлен'
        # Если есть ссылка для отправки данных, то отправляем запрос по ней
        if callback:
            try:
                r = requests.post(url=callback, json=result)
                msg = 'Запрос успешно отправлен'
            except requests.exceptions.ConnectionError as e:
                msg = f'При отправке заказа произошла ошибка {e}'
        # Иначе просто выводим список деталей на экран
        return render_template('order_result.html',
                               title='Заказ успешно создан',
                               details=result['list_detail'],
                               type=set_type,
                               callback=callback_string,
                               request_message=msg)
    else:
        return render_template('order_form.html', title=f'Заказ деталей {set_name}',
                               type=set_type, details=data, callback=callback_string)
